FROM maven:3.8.5-ibmjava-8-alpine as system

WORKDIR /usr/src/app
COPY . .

RUN mvn dependency:go-offline


# Build
FROM maven:3.8.5-openjdk-8 as build

COPY --from=system /usr/src/app /usr/src/app
COPY --from=system /root/.m2 /root/.m2

WORKDIR /usr/src/app
RUN mvn package


# App
FROM openjdk:8-jdk as app

COPY --from=build /usr/src/app/target/LabWeb*.jar /usr/opt/app/app.jar

CMD ["java", "-jar", "/usr/opt/app/app.jar"]

